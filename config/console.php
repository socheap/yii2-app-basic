<?php
Yii::setAlias ( '@tests', dirname ( __DIR__ ) . '/tests' );
Yii::setAlias('@webroot', dirname(__DIR__) . '/web');

$console = [
		'id' => 'basic-console',
		'bootstrap' => [ 'log', 'gii', ],
		'controllerNamespace' => 'app\commands',
		'components' => [
				'cache' => 'yii\caching\FileCache',
				'mutex' => 'yii\mutex\FileMutex',
				'log' => [
						'targets' => [
								[
										'class' => 'yii\log\FileTarget',
										'logFile' => '@runtime/logs/console.log',
										'levels' => [ 'info', 'trace', 'error', 'warning' ]
								]
						]
				],
				/*##########################
				# 2c - Module specific components #
				##########################*/
		],
];

/*########################
 # 4 - Developement Specific Config       #
 #########################*/
if (YII_DEBUG) {

}

return \yii\helpers\ArrayHelper::merge(require(__DIR__ . '/common.php'), $console, require(__DIR__ . '/local/console.php'));
