<?php


namespace app\components;

use yii\helpers\Markdown;

class MarkdownViewAction extends \yii\web\ViewAction
{
    public $viewPrefix = 'markdown';
    public function run()
    {
        $viewName = $this->resolveViewName();

        $controllerLayout = null;
        if ($this->layout !== null) {
            $controllerLayout = $this->controller->layout;
            $this->controller->layout = $this->layout;
        }

        try {
            $output = Markdown::process($this->controller->renderPartial($viewName), 'gfm');

            if ($controllerLayout) {
                $this->controller->layout = $controllerLayout;
            }
        } catch (InvalidParamException $e) {
            if ($controllerLayout) {
                $this->controller->layout = $controllerLayout;
            }

            if (YII_DEBUG) {
                throw new NotFoundHttpException($e->getMessage());
            } else {
                throw new NotFoundHttpException(
                    Yii::t('yii', 'The requested view "{name}" was not found.', ['name' => $viewName])
                );
            }
        }

        return $this->controller->renderContent($output);
    }
}
